import { useState } from "react";
import logo from "./images/draw.png";
import scissor from "./images/scissors.png";
import papper from "./images/papper.png";
import rock from "./images/rock.png";
import win from "./images/win.png";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

function App() {
  const [count, setCount] = useState(0);
  const [NameA, SetNameA] = useState("Chandra");
  const [NameB, SetNameB] = useState("Maria");
  const [ValuaA, SetValueA] = useState("");
  const [ValuaB, SetValueB] = useState("");
  const imageStyle = {
    width: "250px",
  };
  return (
    <div className="App">
      <header className="App-header">
        <div className="row">
          <div className="col">
            <h4 className="text-center">{NameA}</h4>
          </div>
          <div className="col">
            <h4 className="text-center">{NameB}</h4>
          </div>
        </div>
        <div className="option row">
          <div className="col">
            <div className="row">
              <div className="col-4 p-3">
                <button onClick={() => SetValueA("rock")} style={{ background: "none", border: "none" }}>
                  <img src={rock} className="App-logo" alt="logo" />
                </button>
              </div>
              <div className="col-4 p-3">
                <button onClick={() => SetValueA("papper")} style={{ background: "none", border: "none" }}>
                  <img src={papper} className="App-logo" alt="logo" />
                </button>
              </div>
              <div className="col-4 p-3">
                <button onClick={() => SetValueA("scissor")} style={{ background: "none", border: "none" }}>
                  <img src={scissor} className="App-logo" alt="logo" />
                </button>
              </div>
            </div>
          </div>
          <div className="col">
            <div className="row">
              <div className="col-4 p-3">
                <button onClick={() => SetValueB("rock")} style={{ background: "none", border: "none" }}>
                  <img src={rock} className="App-logo" alt="logo" />
                </button>
              </div>
              <div className="col-4 p-3">
                <button onClick={() => SetValueB("papper")} style={{ background: "none", border: "none" }}>
                  <img src={papper} className="App-logo" alt="logo" />
                </button>
              </div>
              <div className="col-4 p-3">
                <button onClick={() => SetValueB("scissor")} style={{ background: "none", border: "none" }}>
                  <img src={scissor} className="App-logo" alt="logo" />
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="hasil">
          <div className="d-flex justify-content-center">
            <div>
              {ValuaA == "rock" && ValuaB == "rock" && (
                <div>
                  <h1 className="text-center">Draw</h1>
                  <img src={logo} alt="" style={imageStyle} />
                </div>
              )}
            </div>
            <div>
              {ValuaA == "rock" && ValuaB == "papper" && (
                <div>
                  <h1 className="text-center">{NameB}</h1>
                  <img src={win} alt="" style={imageStyle} />
                </div>
              )}
            </div>
            <div>
              {ValuaA == "rock" && ValuaB == "scissor" && (
                <div>
                  <h1 className="text-center">{NameA}</h1>
                  <img src={win} alt="" style={imageStyle} />
                </div>
              )}
            </div>
            <div>
              {ValuaA == "papper" && ValuaB == "rock" && (
                <div>
                  <h1 className="text-center">{NameA}</h1>
                  <img src={win} alt="" style={imageStyle} />
                </div>
              )}
            </div>
            <div>
              {ValuaA == "papper" && ValuaB == "papper" && (
                <div>
                  <h1 className="text-center">Draw</h1>
                  <img src={logo} alt="" style={imageStyle} />
                </div>
              )}
            </div>
            <div>
              {ValuaA == "papper" && ValuaB == "scissor" && (
                <div>
                  <h1 className="text-center">{NameB}</h1>
                  <img src={win} alt="" style={imageStyle} />
                </div>
              )}
            </div>
            <div>
              {ValuaA == "scissor" && ValuaB == "rock" && (
                <div>
                  <h1 className="text-center">{NameB}</h1>
                  <img src={win} alt="" style={imageStyle} />
                </div>
              )}
            </div>
            <div>
              {ValuaA == "scissor" && ValuaB == "papper" && (
                <div>
                  <h1 className="text-center">{NameA}</h1>
                  <img src={win} alt="" style={imageStyle} />
                </div>
              )}
            </div>
            <div>
              {ValuaA == "scissor" && ValuaB == "scissor" && (
                <div>
                  <h1 className="text-center">Draw</h1>
                  <img src={logo} alt="" style={imageStyle} />
                </div>
              )}
            </div>
          </div>
        </div>
      </header>
    </div>
  );
}

export default App;
